//
//  ViewExtension.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 02/03/23.
//

import Foundation
import SwiftUI

extension View {
    func toastAlert(isShowing: Binding<Bool>, text: String) -> some View {
        self.modifier(ToastAlert(isShowing: isShowing, text: text))
    }
}
