//
//  Store.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import Foundation

class Store<State>: ObservableObject {
    @Published private(set) var state: State

    init(initial: State) {
        self.state = initial
    }

    func setState(_ newState: State) {
        self.state = newState
    }

    func getState() -> State {
        return state
    }
}
