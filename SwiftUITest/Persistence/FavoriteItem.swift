//
//  FavoriteItem.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 28/02/23.
//

import Foundation

class FavoriteItem: Identifiable, ObservableObject {
    var id = UUID()
    var imeageUrl: String = ""
    var productTitle: String = ""
    var productDescrip: String = ""
    var isFavorite: Bool = false

    init(id: UUID = UUID(), imeageUrl: String, productTitle: String, productDescrip: String, isFavorite: Bool) {
        self.id = id
        self.imeageUrl = imeageUrl
        self.productTitle = productTitle
        self.productDescrip = productDescrip
        self.isFavorite = isFavorite
    }
}
