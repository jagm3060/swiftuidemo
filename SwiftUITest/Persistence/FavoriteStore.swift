//
//  FavoriteStore.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import Foundation

struct FavoriteState {
    var favoriteItem: [FavoriteItem] = []
}


class FavoriteStore: Store<FavoriteState> {

    func setfavoriteItem(_ favoriteItem: [FavoriteItem]) {
        var mutableState = getState()
        mutableState.favoriteItem = favoriteItem
        setState(mutableState)
    }
}
