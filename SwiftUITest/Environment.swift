//
//  Enviroment.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import SwiftUI

struct EnvironmentObjects {
    var favoriteStore: FavoriteStore
    init() {
        self.favoriteStore = FavoriteStore(initial: FavoriteState())
    }
}

struct EnvironmentModifier: ViewModifier {
    static var environment = EnvironmentObjects()

    func body(content: Content) -> some View {
        content
            .environmentObject(Self.environment.favoriteStore)
    }
}
