//
//  ContentView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        RootView()
            .modifier(EnvironmentModifier())
    }
}

struct RootView: View {
    var body: some View {
        VStack {
            CustomeTabView()
                .navigationBarTitleDisplayMode(.inline)
        }
    }
}
