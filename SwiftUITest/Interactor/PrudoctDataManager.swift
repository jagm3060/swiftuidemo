//
//  PrudoctDataManager.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import Foundation
import Combine

class PrudoctDataManager {
    enum WeatherError: Error, CustomStringConvertible {
        case network
        case parsing
        case fetching

        var description: String {
            switch self {
            case .network:
                return "Network error"
            case .parsing:
                return "Parsing error"
            case .fetching:
                return "File fetching error"
            }
        }
    }

    static func currenDataPublisher() -> AnyPublisher<Product, Error> {
        let session = URLSession.shared
        let decoder = JSONDecoder()
        let url = URL(string: "https://fakestoreapi.com/products")
        return session.dataTaskPublisher(for: url!)
                    .map(\.data)
                    .decode(type: Product.self, decoder: decoder)
                    .eraseToAnyPublisher()
    }
}
