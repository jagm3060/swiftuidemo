//
//  HomeModel.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import Foundation

// MARK: - ProductElement
struct ProductElement: Codable, Hashable {

    let id: Int?
    let title: String?
    let price: Double?
    let description: String?
    let category: Category?
    let image: String?
    let rating: Rating?

    public func hash(into hasher: inout Hasher) {
           return hasher.combine(id)
       }

    static func == (lhs: ProductElement, rhs: ProductElement) -> Bool {
        return lhs.id == rhs.id
    }
}

enum Category: String, Codable {
    case electronics = "electronics"
    case jewelery = "jewelery"
    case menSClothing = "men's clothing"
    case womenSClothing = "women's clothing"
}

// MARK: - Rating
struct Rating: Codable {
    let rate: Double?
    let count: Int?
}

typealias Product = [ProductElement]
