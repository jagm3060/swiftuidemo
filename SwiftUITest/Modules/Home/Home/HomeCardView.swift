//
//  HomeCardView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import SwiftUI

struct HomeCardView: View {
    let data: ProductElement?
    var viewModel: HomeViewModel?
    @State private var toogleHeart: Bool = false
    var isActive: Bool = false
    var body: some View {
        let cardAndImageWidth: CGFloat = 175
        let cardHeight: CGFloat = 175
        let cornerRadius: CGFloat = 5
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                .strokeBorder(SwiftUI.Color.gray, lineWidth: 1)
                .frame(width: cardAndImageWidth, height: cardHeight)
                .background(SwiftUI.Color.white)
            VStack(alignment: .center, spacing: 20) {
                HStack(alignment: .center, spacing: 20) {
                    VStack(spacing: 20) {
                        Button(action: {}) {
                            Image(systemName: "plus")
                        }
                        Button(action: {}) {
                            Image(systemName: "minus")
                        }
                        Button(action: {}) {
                            Image(systemName: "square.and.arrow.up")
                        }
                    }
                    AsyncImage(url: URL(string: data?.image ?? "")) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 70, height: 70)
                    .clipped()
                    Button(action: {
                        toogleHeart.toggle()
                        if toogleHeart {
                            viewModel?.addFavorite(imeageUrl: data?.image ?? "", productTitle: data?.title ?? "", productDescrip: data?.description ?? "", isFavorite: true)
                            viewModel?.showToast = true
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                withAnimation {
                                    viewModel?.showToast = false
                                }
                            }
                        } else {
                            viewModel?.removeFromList(name: data?.title ?? "")
                        }
                    }) {
                        if toogleHeart && isActive  {
                            Image(systemName: "heart.fill")
                                .foregroundColor(.red)
                        }else {
                            Image(systemName: "heart")
                        }
                    }
                }
                VStack(spacing: 10) {
                    Text(data?.description ?? "")
                        .font(.system(size: 12))
                        .lineLimit(3)
                    HStack(alignment: .center) {
                        Text("$\(String(format: "%.2f", data?.price ?? 0.0))")
                            .font(.system(size: 12))
                            .lineLimit(1)
                        Spacer()
                        Text("\(String(format: "%.2f", data?.rating?.rate ?? 0.0))")
                            .font(.system(size: 12))
                            .lineLimit(1)
                        Image(systemName: "star.fill")
                            .foregroundColor(.yellow)
                    }
                }
                .padding(.horizontal, 10)
            }
            .padding(.vertical, 10)
        }
        .frame(width: cardAndImageWidth, height: cardHeight)
        .cornerRadius(cornerRadius)
    }
}

struct HomeCardView_Previews: PreviewProvider {
    static var previews: some View {
        HomeCardView(data: ProductElement(id: 1, title: "Title", price: 25.0, description: "Description", category: Category(rawValue: "Category"), image: "", rating: Rating(rate: 5.0, count: 1)), viewModel: HomeViewModel())
    }
}
