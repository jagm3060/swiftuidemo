//
//  HomeViewModel.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    var favoriteStore: FavoriteStore?
    var favoriteArray = [FavoriteItem]()
    @Published var productData: Product?
    @Published var showToast: Bool = false
    private var subsriptions = Set<AnyCancellable>()
    func fetchData() {
        PrudoctDataManager.currenDataPublisher()
            .map(Product.init)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { response in
                switch response {
                case .failure(let error):
                    print("Failed with error: \(error)")
                    return
                case .finished:
                    print("Succeesfully finished!")
                }
            }, receiveValue: { value in
                self.productData = value
            })
            .store(in: &subsriptions)
    }
    func addFavorite(imeageUrl: String, productTitle: String, productDescrip: String, isFavorite: Bool) {
        favoriteArray = favoriteStore?.state.favoriteItem ?? []
        favoriteArray.append(FavoriteItem(imeageUrl: imeageUrl, productTitle: productTitle, productDescrip: productDescrip, isFavorite: true))
        self.favoriteStore?.setfavoriteItem(favoriteArray)
    }

    func removeFromList(name: String) {
        favoriteArray = favoriteStore?.state.favoriteItem ?? []
        if let selectedIdex = favoriteStore?.state.favoriteItem.firstIndex(where: {$0.productTitle == name}){
            favoriteArray.remove(at: selectedIdex)
        }
        favoriteStore?.setfavoriteItem(favoriteArray)
    }

    func getIsFavorite(name: String) -> Bool {
        if let store = self.favoriteStore  {
            if store.state.favoriteItem.contains(where: {$0.productTitle == name}) {
                return true
            }else {
               return false
            }
        }
        return false
    }
}
