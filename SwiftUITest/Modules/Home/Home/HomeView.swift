//
//  HomeView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import SwiftUI

struct HomeView<Model>: View where Model: HomeViewModel {
    @ObservedObject private var viewModel: Model
    @EnvironmentObject var favoriteStore: FavoriteStore
    init(viewModel: Model) {
        self.viewModel = viewModel
        viewModel.fetchData()
    }
    var body: some View {
        let columns = [
            GridItem(.fixed(200)),
            GridItem(.flexible()),
        ]
        VStack {
            ScrollView {
                LazyVGrid(columns: columns, spacing: 10) {
                    let data = viewModel.productData ?? []
                    ForEach(data, id: \.self) { i in
                        HomeCardView(data: i, viewModel: viewModel, isActive: viewModel.getIsFavorite(name: i.title ?? ""))
                    }
                }
            }
            .padding([.trailing], 10)
            .toastAlert(isShowing: $viewModel.showToast, text: "Producto Agregado a Favoritos")
        }
        .onAppear {
            viewModel.favoriteStore = self.favoriteStore
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: HomeViewModel())
    }
}
