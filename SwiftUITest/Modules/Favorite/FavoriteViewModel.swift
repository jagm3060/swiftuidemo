//
//  FavoriteViewModel.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import Foundation

class FavoriteViewModel: ObservableObject {
    var favoriteStore: FavoriteStore?
    var dataArray = [FavoriteItem]()

    func removeFromList(id: UUID) {
        dataArray = favoriteStore?.state.favoriteItem ?? []
        if let selectedIdex = favoriteStore?.state.favoriteItem.firstIndex(where: {$0.id == id}){
            dataArray.remove(at: selectedIdex)
        }
        favoriteStore?.setfavoriteItem(dataArray)
    }
}
