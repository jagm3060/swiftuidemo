//
//  FavoriteCardView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 28/02/23.
//

import SwiftUI

struct FavoriteCardView: View {
    let data: FavoriteItem?
    var body: some View {
        HStack {
            AsyncImage(url: URL(string: data?.imeageUrl ?? "")) { image in
                image.resizable()
            } placeholder: {
                ProgressView()
            }
            .frame(width: 70, height: 70)
            .clipped()
            VStack(alignment: .leading ,spacing: 10) {
                Text(data?.productDescrip ?? "")
                    .font(.system(size: 12))
                    .lineLimit(2)
                Text("$\(String(format: "%.2f", 1.5))")
                    .font(.system(size: 12))
                    .lineLimit(1)
            }
        }
    }
}

struct FavoriteCardView_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteCardView(data: FavoriteItem(imeageUrl: "", productTitle: "", productDescrip: "", isFavorite: true))
    }
}
