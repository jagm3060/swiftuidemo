//
//  FavoriteView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import SwiftUI

struct FavoriteView<Model>: View where Model: FavoriteViewModel {
    @ObservedObject private var viewModel: Model
    @EnvironmentObject var favoriteStore: FavoriteStore
    init(viewModel: Model) {
        self.viewModel = viewModel
    }
    var body: some View {
        VStack {
            List {
                ForEach(favoriteStore.state.favoriteItem) { i in
                    FavoriteCardView(data: i)
                        .swipeActions(allowsFullSwipe: false) {
                            Button(role: .destructive) {
                                viewModel.removeFromList(id: i.id)
                            } label: {
                                Label("Remove", systemImage: "trash.fill")
                            }

                            Button {

                            } label: {
                                Label("Share", systemImage: "square.and.arrow.up.fill")
                            }
                            .tint(.indigo)
                        }
                }
            }
            .onAppear{
                viewModel.favoriteStore = self.favoriteStore
            }
        }
    }
}

struct FavoriteView_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteView(viewModel: FavoriteViewModel())
            .environmentObject(FavoriteStore(initial: FavoriteState()))
    }
}
