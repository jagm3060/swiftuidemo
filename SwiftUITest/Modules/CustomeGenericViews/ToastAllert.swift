//
//  ToastAllert.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 02/03/23.
//

import Foundation
import SwiftUI

struct ToastAlert: ViewModifier {
    @Binding var isShowing: Bool
    let text: String

    func body(content: Content) -> some View {
        ZStack(alignment: .top) {
            content
            VStack {
                Text(text)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.green.opacity(0.7))
                    .cornerRadius(8)
                    .padding(.bottom, UIApplication.shared.currentUIWindow()?.rootViewController?.additionalSafeAreaInsets.bottom)
                    .opacity(isShowing ? 1 : 0)
                    .animation(Animation.easeInOut(duration: 0.3), value: 200.0)
                Spacer()
            }
        }
    }
}


