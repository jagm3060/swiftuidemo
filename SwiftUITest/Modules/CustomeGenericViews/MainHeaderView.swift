//
//  MainHeaderView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 28/02/23.
//

import SwiftUI

struct MainHeaderView: View {
    let title: String
    let image: String
    var body: some View {
        HStack(alignment: .center) {
            Text(title)
                .foregroundColor(.white)
                .font(.system(size: 30))
                .bold()
            Image(systemName: image)
                .foregroundColor(.white)
        }
        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity)
        .frame(height: 40)
        .background(Color.blue)
    }
}

struct MainHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        MainHeaderView(title: "Home", image: "house.fill")
    }
}
