//
//  TabView.swift
//  GlobantSwiftUITest
//
//  Created by Jorge Alberto Garcia Mercado on 24/02/23.
//

import SwiftUI

struct CustomeTabView: View {
    var body: some View {
        TabView {
            VStack {
                MainHeaderView(title: "Home", image: "house.fill")
                HomeView(viewModel: HomeViewModel())
            }
            .tabItem {
                Image(systemName: "house.fill")
                Text("Home Tab")
            }
            VStack {
                MainHeaderView(title: "Favorite", image: "heart.fill")
                FavoriteView(viewModel: FavoriteViewModel())
            }
            .tabItem {
                Image(systemName: "heart.fill")
                Text("Favorite Tab")
            }
        }
    }
}

struct CustomeTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomeTabView()
            .environmentObject(FavoriteStore(initial: FavoriteState()))
    }
}
